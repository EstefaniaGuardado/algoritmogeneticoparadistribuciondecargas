/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmogenetico;

import java.util.Random;

/**
 *
 * @author estefaniaguardado
 */
public class AlgoritmoGenetico {

    public Random ran;
    public int tareas[];
    private int replicas[];

    private int poblacion[][];
    public float acumuladorReplica[][];
    private float cargaDePoblacion[];
    private float suma_carga;
    private float promedio;

    public int INDIVIDUOS_DE_POBLACION = 10;
    public int NUMERO_GENES_POR_CROMOSOMA_MAX = 24;
    public int NUMERO_DE_GENERACIONES = 10;
    public int CARGAS_POR_TAREA = 8;
    public int CAPACIDAD_POR_PROCESADOR = 8;

    private static final int GEN_A = 1;
    private static final int GEN_B = 0;

    public static final double PROBABILIDAD_DE_MUTACION = 0.6;
    private static final double PROBABILIDAD_DE_CRUZAMIENTO = 0.7;

    public AlgoritmoGenetico() {
        ran = new Random();
        replicas = new int[CARGAS_POR_TAREA];
        tareas = new int[CARGAS_POR_TAREA];
    }

    public void procesaAlgoritmo() {

        inicializacionDeEstructuras();
        asignacionDeCargaALasTareas();
        asignacionDeVelocidadALasReplicas();
        obtenerPoblacionInicial();

        System.out.println("Poblacion inicial");
        imprimeGeneracion(poblacion);

        inicializarAcumuladorDeReplicas(acumuladorReplica);
        obtenerCargaDeTareasPorReplica(poblacion);
        calcularCarga(cargaDePoblacion);

        inicializarAcumuladorDeReplicas(acumuladorReplica);
        obtenerCargaDeTareasPorReplica(poblacion);
        ordenaPoblacionAscendentemente(poblacion, cargaDePoblacion);
        
        System.out.println("Carga Poblacion");
        imprimeCarga(cargaDePoblacion);
        System.out.println();
        
        int contadorDeGeneracion = 0;
        do {
            suma_carga = sumaTotalPoblacion();
            promedio = obtenPromedioDeCargaDeTareas();

            int seleccionadoElitista = obtenerPosicionDelElitista(cargaDePoblacion);
            int elitista[] = obtenerElMejorIndivido(seleccionadoElitista);

            int primerPadreSeleccionado = seleccionDeLosPadresDeLaPoblacion(cargaDePoblacion);
            int segundoPadreSeleccionado = seleccionDeLosPadresDeLaPoblacion(cargaDePoblacion);

            int padre[][] = obtenerLosPadresDeLaPoblacion(primerPadreSeleccionado, segundoPadreSeleccionado);
            int generacionNueva[][] = obtenerNuevaGeneracion(elitista, padre);
            int generacionCruzada[][] = cruzamientoDeLaPoblacion(generacionNueva);
            int generacionMutada[][] = mutarLaPoblacion(generacionCruzada);
            poblacion = generacionMutada;
            inicializarAcumuladorDeReplicas(acumuladorReplica);
            obtenerCargaDeTareasPorReplica(poblacion);
            calcularCarga(cargaDePoblacion);
            ordenaPoblacionAscendentemente(poblacion, cargaDePoblacion);


            System.out.println("Generación " + String.valueOf(contadorDeGeneracion));
            imprimeGeneracion(poblacion);

            System.out.println("Carga Poblacion");
            imprimeCarga(cargaDePoblacion);
            System.out.println();

            System.out.println("-------------------------------------------------");
        } while (++contadorDeGeneracion < NUMERO_DE_GENERACIONES);
        
        suma_carga = sumaTotalPoblacion();
        promedio = obtenPromedioDeCargaDeTareas();
            
        int seleccionadoElitista = obtenerPosicionDelElitista(cargaDePoblacion);
        int elitista[] = obtenerElMejorIndivido(seleccionadoElitista);
        
        System.out.println("La mejor distribucion es: ");
        System.out.println();
        for (int i=0; i<NUMERO_GENES_POR_CROMOSOMA_MAX; i++)
                System.out.print(elitista[i] + " ");
        
        System.out.println();
        System.out.println("Su carga es: ");
        System.out.print(cargaDePoblacion[seleccionadoElitista]);
        System.out.println();
    }

    private void imprimeGeneracion(int[][] generacionInteres) {
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; j++) {
                System.out.print(generacionInteres[i][j] + " ");
            }
            System.out.println();
        }
    }

    private void imprimeCarga(float[] cargaDePoblacion) {
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            System.out.println(cargaDePoblacion[i]);
        }
    }

    public int[][] mutarLaPoblacion(int generacionCruzadaInicial[][]) {
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; j++) {
                float aleatorio = (ran.nextFloat());
                if (aleatorio < PROBABILIDAD_DE_MUTACION) {
                    int nuevoGen = generacionCruzadaInicial[i][j] == GEN_A ? GEN_B : GEN_A;
                    generacionCruzadaInicial[i][j] = nuevoGen;
                }
            }
        }
        return generacionCruzadaInicial;
    }

    public int[][] cruzamientoDeLaPoblacion(int generacionNueva[][]) {

        int cruzamiento[][] = new int[2][NUMERO_GENES_POR_CROMOSOMA_MAX];

        int individuoBase = 0;
        int posicionHijo = 3;
        boolean cruzado = true;

        while (individuoBase < INDIVIDUOS_DE_POBLACION && cruzado) {
            float indiceDeCruzamiento = (ran.nextFloat());
            int puntoDeCruzamiento = indiceDeCruzamiento < PROBABILIDAD_DE_CRUZAMIENTO ? ran.nextInt(NUMERO_GENES_POR_CROMOSOMA_MAX) : 0;

            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
                int pos1 = j < puntoDeCruzamiento ? individuoBase + 1 : individuoBase;
                int pos2 = j < puntoDeCruzamiento ? individuoBase : individuoBase + 1;

                cruzado = generacionNueva.length > pos1 && generacionNueva.length > pos2;
                if (cruzado) {
                    cruzamiento[0][j] = generacionNueva[pos1][j];
                    cruzamiento[1][j] = generacionNueva[pos2][j];
                }
            }

            if (cruzado) {
                __llenaCromosomaDeHijo(generacionNueva, posicionHijo++, cruzamiento, 0);
                __llenaCromosomaDeHijo(generacionNueva, posicionHijo++, cruzamiento, 1);
            }

            individuoBase++;
        }

        return generacionNueva;
    }

    private void __llenaCromosomaDeHijo(int[][] generacionNueva1, int posicionHijo, int[][] cruzamiento, int posicionCruzamiento) {
        if (generacionNueva1.length > posicionHijo) {
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; j++) {
                generacionNueva1[posicionHijo][j] = cruzamiento[posicionCruzamiento][j];
            }
        }
    }

    private int[][] obtenerNuevaGeneracion(int[] elitista, int padre[][]) {
        int generacion[][] = new int[INDIVIDUOS_DE_POBLACION][NUMERO_GENES_POR_CROMOSOMA_MAX];
        int posicionInteres = 0;

        for (int j = 0; j < elitista.length; ++j) {
            generacion[posicionInteres][j] = elitista[j];
        }
        posicionInteres++;

        for (int i = 0; i < padre.length; ++i) {
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; j++) {
                generacion[posicionInteres][j] = padre[i][j];
            }
            posicionInteres++;
        }
        return generacion;
    }

    private int seleccionDeLosPadresDeLaPoblacion(float[] cargaDePoblacion) {
        int seleccionado = 0;
        double califAcumulada = 0;
        double aleatorio = ran.nextFloat();

        aleatorio = aleatorio * suma_carga;
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            califAcumulada += cargaDePoblacion[i];
            if (califAcumulada > aleatorio) {
                seleccionado = --i;
                break;
            }
        }
        return seleccionado >= 0 ? seleccionado : 0;
    }

    private int[][] obtenerLosPadresDeLaPoblacion(int seleccionado, int segundoPadreSeleccion) {
        int padre[][] = new int[2][NUMERO_GENES_POR_CROMOSOMA_MAX];

        for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {
            padre[0][j] = poblacion[seleccionado][j];
            padre[1][j] = poblacion[segundoPadreSeleccion][j];
        }

        return padre;
    }

    private int obtenerPosicionDelElitista(float[] cargaDePoblacion) {
        int seleccionado = 0;
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            if (cargaDePoblacion[i] > promedio) {
                seleccionado = --i;
                break;
            }
        }
        return seleccionado >= 0 ? seleccionado : 0;
    }

    private int[] obtenerElMejorIndivido(int seleccionado) {
        int[] elitista = new int[NUMERO_GENES_POR_CROMOSOMA_MAX];
        for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; j++) {
            elitista[j] = poblacion[seleccionado][j];
        }
        return elitista;
    }

    public int[][] ordenaPoblacionAscendentemente(int[][] generacionNueva, float[] cargaDePoblacion) {
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION - 1; i++) {
            for (int j = 0; j < INDIVIDUOS_DE_POBLACION - 1; j++) {
                if (cargaDePoblacion[j] > cargaDePoblacion[j + 1]) {
                    float cargaTemporal = cargaDePoblacion[j];
                    cargaDePoblacion[j] = cargaDePoblacion[j + 1];
                    cargaDePoblacion[j + 1] = cargaTemporal;

                    for (int a = 0; a < NUMERO_GENES_POR_CROMOSOMA_MAX; a++) {
                        int genTemporal = generacionNueva[j][a];
                        generacionNueva[j][a] = generacionNueva[j + 1][a];
                        generacionNueva[j + 1][a] = genTemporal;
                    }
                }
            }
        }
        return generacionNueva;
    }

    private float sumaTotalPoblacion() {
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            suma_carga += cargaDePoblacion[i];
        }
        return suma_carga;
    }

    private int[][] obtenerPoblacionInicial() {
        int random;
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; i++) {
            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; j++) {
                random = (ran.nextInt(2));
                poblacion[i][j] = random;
            }
        }
        return poblacion;
    }

    public float[][] obtenerCargaDeTareasPorReplica(int[][] poblacion) {
        int sumaDeBits = 0;
        int numeroDeIteraciones = NUMERO_GENES_POR_CROMOSOMA_MAX / CARGAS_POR_TAREA;

        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
            int numero_tarea = 0;
            double numeroDeIteracionesActual = -1;

            for (int j = 0; j < NUMERO_GENES_POR_CROMOSOMA_MAX; ++j) {

                if (numeroDeIteracionesActual < 0) {
                    numeroDeIteracionesActual = numeroDeIteraciones - 1;
                    sumaDeBits = 0;
                }

                double cantidadASumar = Math.pow(2, numeroDeIteracionesActual--);
                int genActual = poblacion[i][j];
                sumaDeBits += genActual * cantidadASumar;

                if (numeroDeIteracionesActual < 0) {
                    int carga = tareas[numero_tarea++];
                    acumuladorReplica[i][sumaDeBits] += carga;
                }
            }
        }
        return acumuladorReplica;
    }

    private float[][] inicializarAcumuladorDeReplicas(float[][] acumuladorReplica) {
        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
            for (int j = 0; j < CAPACIDAD_POR_PROCESADOR; ++j) {
                acumuladorReplica[i][j] = 0;
            }
        }

        return acumuladorReplica;
    }

    private void inicializacionDeEstructuras() {
        poblacion = new int[INDIVIDUOS_DE_POBLACION][NUMERO_GENES_POR_CROMOSOMA_MAX];
        cargaDePoblacion = new float[INDIVIDUOS_DE_POBLACION];
        acumuladorReplica = new float[INDIVIDUOS_DE_POBLACION][CAPACIDAD_POR_PROCESADOR];
    }

    private float obtenPromedioDeCargaDeTareas() {
        float suma_totales = 0;
        for (int i = 0; i < CARGAS_POR_TAREA; i++) {
            suma_totales = tareas[i] + suma_totales;
        }
        promedio = suma_totales / CARGAS_POR_TAREA;
        return promedio;
    }

    private void asignacionDeVelocidadALasReplicas() {
        int random;
        for (int i = 0; i < CAPACIDAD_POR_PROCESADOR; i++) {
            random = (ran.nextInt(4) + 1);
            replicas[i] = random;
        }
    }

    private void asignacionDeCargaALasTareas() {
        int random;
        for (int i = 0; i < CARGAS_POR_TAREA; i++) {
            random = (ran.nextInt(50) + 1);
            tareas[i] = random;
        }
    }

    private float[] calcularCarga(float[] cargaDePoblacion) {

        for (int i = 0; i < INDIVIDUOS_DE_POBLACION; ++i) {
            float calificacionAcumulada = 0;
            for (int j = 0; j < replicas.length; j++) {
                float capacidadReplica = replicas[j];
                float sumaTotalAcumuladorReplicas = acumuladorReplica[i][j] / capacidadReplica;
                calificacionAcumulada += sumaTotalAcumuladorReplicas;
            }
            cargaDePoblacion[i] = calificacionAcumulada;
        }
        return cargaDePoblacion;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AlgoritmoGenetico algoritmoGenetico = new AlgoritmoGenetico();
        algoritmoGenetico.procesaAlgoritmo();
    }

}
