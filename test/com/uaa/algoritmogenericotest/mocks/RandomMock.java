/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uaa.algoritmogenericotest.mocks;

/**
 *
 * @author estefaniaguardado
 */
public class RandomMock extends java.util.Random {

    public float RETURN_VALUE;
    public int NEXT_INT_VALUE;
    
    @Override
    public float nextFloat() {
        return RETURN_VALUE;
    }

    @Override
    public int nextInt(int bound) {
        return NEXT_INT_VALUE;
    }    
}
