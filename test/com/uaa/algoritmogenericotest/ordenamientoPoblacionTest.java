/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uaa.algoritmogenericotest;

import com.uaa.algoritmogenericotest.utils.UtilidadesDeMatrices;
import junit.framework.TestCase;

/**
 *
 * @author estefaniaguardado
 */
public class ordenamientoPoblacionTest extends TestCase {
    private algoritmogenetico.AlgoritmoGenetico sut;
    
    public ordenamientoPoblacionTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        sut = new algoritmogenetico.AlgoritmoGenetico();
    }

    // TODO add test methods here. The name must begin with 'test'. For example:
    public void testConsiderandoUnaMatriz() {
        sut.INDIVIDUOS_DE_POBLACION = 3;
        sut.NUMERO_GENES_POR_CROMOSOMA_MAX = 2;
        
        int[][] generacionNueva = new int[][] {
            {0, 1},
            {2, 3},
            {4, 5}
        };
        float[] cargaPoblacion = new float[] {
            5, 4, 6
        };
        
        sut.ordenaPoblacionAscendentemente(generacionNueva, cargaPoblacion);
        
        int[][] generacionEsperada = new int[][] {
            {2, 3},
            {0, 1},
            {4, 5}
        };
        
        float[] cargaEsperada = new float[] {
            4, 5, 6
        };
        
        UtilidadesDeMatrices.matrizIgual(generacionEsperada, generacionNueva);
        UtilidadesDeMatrices.vectorIgual(cargaEsperada, cargaPoblacion);
    }
    
    public void testErrorEnOrdenamientoEnEjecucion() {
        sut.INDIVIDUOS_DE_POBLACION = 10;
        sut.NUMERO_GENES_POR_CROMOSOMA_MAX = 24;
        
        int[][] generacionNueva = new int[][] {
            {1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1},
            { 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0 },
            { 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1 },
            { 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1 },
            { 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1 },
            { 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0 },
            { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0 },
            { 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1 }
        };
        
        float[] cargaPoblacion = new float[] {
            60,
            68,
            71,
            75,
            56,
            112,
            98,
            58,
            154,
            82
        };
        
        sut.ordenaPoblacionAscendentemente(generacionNueva, cargaPoblacion);
        
        int[][] generacionEsperada = new int[][] {
            { 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1 },
            { 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0 },
            {1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1},
            { 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0 },
            { 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1 },
            { 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1 },
            { 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1 },
            { 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0 },
        };
        
        float[] cargaEsperada = new float[] {
            56,
            58,
            60,
            68,
            71,
            75,
            82,
            98,
            112,
            154
        };
        
        UtilidadesDeMatrices.matrizIgual(generacionEsperada, generacionNueva);
        UtilidadesDeMatrices.vectorIgual(cargaEsperada, cargaPoblacion);
    }
}
