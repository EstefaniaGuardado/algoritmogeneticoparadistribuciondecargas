/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uaa.algoritmogenericotest;

import com.uaa.algoritmogenericotest.mocks.RandomMock;
import com.uaa.algoritmogenericotest.utils.UtilidadesDeMatrices;
import junit.framework.TestCase;

/**
 *
 * @author estefaniaguardado
 */
public class MutacionTest extends TestCase {
    
    private RandomMock randomMock;
    private algoritmogenetico.AlgoritmoGenetico sut;
    private int[][] generacionCruzadaI;
    
    public MutacionTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        sut = new algoritmogenetico.AlgoritmoGenetico();
        randomMock = new RandomMock();
        
        generacionCruzadaI = new int[sut.INDIVIDUOS_DE_POBLACION][sut.NUMERO_GENES_POR_CROMOSOMA_MAX];
    }

    public void testDadaUnaMatrizCruzadaDeCerosSiElRandomSiempreEsMayorALaProbabilidadDeCruzamientoLaMatrizResultadoEsLaMisma() {
        sut.ran = randomMock;
        randomMock.RETURN_VALUE = (float) 1;
        
        UtilidadesDeMatrices.poblaMatriz(generacionCruzadaI, 0, sut.INDIVIDUOS_DE_POBLACION, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
        sut.mutarLaPoblacion(generacionCruzadaI);
        
        UtilidadesDeMatrices.matrizConValor(generacionCruzadaI, 0, sut.INDIVIDUOS_DE_POBLACION, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
    }
    
    public void testDadaUnaMatrizCruzadaDeCerosSiElRandomSiempreEsMenorALaProbabilidaDeCruzamientoMatrizResultadoEstaInvertida() {
        sut.ran = randomMock;
        randomMock.RETURN_VALUE = (float) 0;
        
        UtilidadesDeMatrices.poblaMatriz(generacionCruzadaI, 0, sut.INDIVIDUOS_DE_POBLACION, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
        sut.mutarLaPoblacion(generacionCruzadaI);
        
        UtilidadesDeMatrices.matrizConValor(generacionCruzadaI, 1, sut.INDIVIDUOS_DE_POBLACION, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
    }

    
}
