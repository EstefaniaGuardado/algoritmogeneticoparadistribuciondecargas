/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uaa.algoritmogenericotest;

import com.uaa.algoritmogenericotest.utils.UtilidadesDeMatrices;
import junit.framework.TestCase;

/**
 *
 * @author estefaniaguardado
 */
public class ObtencionCargaDeTareasPorReplicaTest extends TestCase {
    
    private algoritmogenetico.AlgoritmoGenetico sut;
    
    public ObtencionCargaDeTareasPorReplicaTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        sut = new algoritmogenetico.AlgoritmoGenetico();
    }

    public void testDadaUnaPoblacionDeUnIndividuoConUnCromosomadeDeUnos() {
        sut.INDIVIDUOS_DE_POBLACION = 1;
        sut.NUMERO_GENES_POR_CROMOSOMA_MAX = 3;
        sut.CARGAS_POR_TAREA = 1;
        sut.tareas = new int[] {
            5
        };
        sut.acumuladorReplica = new float[][] {
            {
                0, 0, 0, 0, 0, 0, 0, 0
            }
        };
        
        int[][] poblacion = new int[][] {
            {
                1, 1, 1
            }
        };
        
        float[][] cargaAEvaluar = sut.obtenerCargaDeTareasPorReplica(poblacion);
        
        float[][] cargaEsperada = new float[][] {
            {
                0, 0, 0, 0, 0, 0, 0, 5
            }
        };
        
        UtilidadesDeMatrices.matrizIgual(cargaEsperada, cargaAEvaluar);
    }
    
    public void testDadaUnaPoblacionDeTresIndividuoConTresCromosomadeDeDosUnosUnCero() {
        sut.INDIVIDUOS_DE_POBLACION = 3;
        sut.NUMERO_GENES_POR_CROMOSOMA_MAX = 6;
        sut.CARGAS_POR_TAREA = 2;
        sut.tareas = new int[] {
            5, 8
        };
        sut.acumuladorReplica = new float[][] {
            {
                0, 0, 0, 0, 0, 0, 0, 0
            },
            {
                0, 0, 0, 0, 0, 0, 0, 0
            },
            {
                0, 0, 0, 0, 0, 0, 0, 0
            }
        };
        
        int[][] poblacion = new int[][] {
            {
                1, 1, 0, 0, 1, 1
            },
            {
                1, 1, 0, 1, 1, 0
            },
            {
                0, 1, 1, 0, 1, 0
            }
        };
        
        float[][] cargaAEvaluar = sut.obtenerCargaDeTareasPorReplica(poblacion);
        
        float[][] cargaEsperada = new float[][] {
            {
                0, 0, 0, 8, 0, 0, 5, 0
            },
            {
                0, 0, 0, 0, 0, 0, 13, 0
            },
            {
                0, 0, 8, 5, 0, 0, 0, 0
            }
        };
        
        UtilidadesDeMatrices.matrizIgual(cargaEsperada, cargaAEvaluar);
    }
}
