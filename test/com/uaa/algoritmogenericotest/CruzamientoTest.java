/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uaa.algoritmogenericotest;

import com.uaa.algoritmogenericotest.mocks.RandomMock;
import com.uaa.algoritmogenericotest.utils.UtilidadesDeMatrices;
import junit.framework.TestCase;

/**
 *
 * @author estefaniaguardado
 */
public class CruzamientoTest extends TestCase {
    
    private RandomMock randomMock;
    private algoritmogenetico.AlgoritmoGenetico sut;
    private int[][] generacionCruzadaI;
    
    public CruzamientoTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        sut = new algoritmogenetico.AlgoritmoGenetico();
        randomMock = new RandomMock();
        
        sut.INDIVIDUOS_DE_POBLACION = 5;
        sut.NUMERO_GENES_POR_CROMOSOMA_MAX = 4;
        
        generacionCruzadaI = new int[sut.INDIVIDUOS_DE_POBLACION][sut.NUMERO_GENES_POR_CROMOSOMA_MAX];
    }

    public void testDadoDosIndividuosInversosSiLaProbabilidadDeCruzamientoEsMenorQueElAleatorioNoSeCruzan() {
    
        sut.ran = randomMock;
        randomMock.RETURN_VALUE = 0;
        randomMock.NEXT_INT_VALUE = 2;
        
        UtilidadesDeMatrices.poblaMatriz(generacionCruzadaI, 0, sut.INDIVIDUOS_DE_POBLACION, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
        UtilidadesDeMatrices.actualizaRenglonConValor(generacionCruzadaI, 1, 1, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
        
        sut.cruzamientoDeLaPoblacion(generacionCruzadaI);
        
        int[][] expected = new int[][] {
            {
                0, 0, 0, 0
            },
            {
                1, 1, 1, 1
            },
            {
                0, 0, 0, 0
            },
            {
                1, 1, 0, 0
            },
            {
                0, 0, 1, 1
            }
        };
        
        UtilidadesDeMatrices.matrizIgual(expected, generacionCruzadaI);
    }
    
    public void testDadoDosIndividuosInversosDondeElPrimeroEsDe1SeMezclaCorrectamenteElHijoPrimeroAPartirDeLaSegundaPosicion() {
        sut.ran = randomMock;
        randomMock.RETURN_VALUE = 0;
        randomMock.NEXT_INT_VALUE = 2;
        
        UtilidadesDeMatrices.poblaMatriz(generacionCruzadaI, 0, sut.INDIVIDUOS_DE_POBLACION, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
        UtilidadesDeMatrices.actualizaRenglonConValor(generacionCruzadaI, 0, 1, sut.NUMERO_GENES_POR_CROMOSOMA_MAX);
        
        sut.cruzamientoDeLaPoblacion(generacionCruzadaI);
        
        int[][] expected = new int[][] {
            {
                1, 1, 1, 1
            },
            {
                0, 0, 0, 0
            },
            {
                0, 0, 0, 0
            },
            {
                0, 0, 1, 1
            },
            {
                1, 1, 0, 0
            }
        };
        
        UtilidadesDeMatrices.matrizIgual(expected, generacionCruzadaI);
    }
}
