/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uaa.algoritmogenericotest.utils;

import static junit.framework.Assert.assertEquals;

/**
 *
 * @author estefaniaguardado
 */
public class UtilidadesDeMatrices {
    
    public static void poblaMatriz(int[][] matriz, int valor, int renglones, int columnas) {
        for(int i = 0; i < renglones; ++i) {
            for (int j = 0; j < columnas; ++j) {
                matriz[i][j] = valor;
            }
        }
    }

    public static void matrizConValor(int[][] matriz, int valor, int renglones, int columnas) {
        for(int i = 0; i < renglones; ++i) {
            for (int j = 0; j < columnas; ++j) {
                assertEquals(valor, matriz[i][j]);
            }
        }
    }

    public static void actualizaRenglonConValor(int[][] matriz, int renglon, int valor, int columnas) {
        for (int j = 0; j < columnas; ++j) {
            matriz[renglon][j] = valor;
        }
    }

    public static void matrizIgual(int[][] expected, int[][] matriz) {
        assertEquals("Numero de renglones no coinciden", expected.length, matriz.length);
        for(int i = 0; i < expected.length; ++i) {
            assertEquals("Numero de columnas no coinciden", expected[i].length, matriz[i].length);
            
            for (int j = 0; j < expected[i].length; ++j) {
                assertEquals("Posicion[" + String.valueOf(i) + "][" + String.valueOf(j) + "]", expected[i][j], matriz[i][j]);
            }
        }
    }
    
    public static void matrizIgual(float[][] expected, float[][] matriz) {
        assertEquals("Numero de renglones no coinciden", expected.length, matriz.length);
        for(int i = 0; i < expected.length; ++i) {
            assertEquals("Numero de columnas no coinciden", expected[i].length, matriz[i].length);
            
            for (int j = 0; j < expected[i].length; ++j) {
                assertEquals(expected[i][j], matriz[i][j]);
            }
        }
    }

    public static void vectorIgual(float[] expected, float[] vector) {
        for (int j = 0; j < expected.length; ++j) {
            assertEquals(expected[j], vector[j]);
        }
    }
}
