# Genetic Algorithm for Load Balancing #

This project is developed in Java. It’s a genetic algorithm that improves the load balancing between computers with different or equal capacities.

This algorithm have like parameters the capacity for load of each computer, for process and calculate the best distribution of load between all computers, using and implementing the basics of genetic algorithm, all of this through of a user interface.

Contributors: 

- Dafne Pinedo (dafne.pinedo@gmail.com)

- Ivan Amaury Rodriguez (iamauryrov93@gmail.com)